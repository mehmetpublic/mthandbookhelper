# MT Handbook Helper #
MT Handbook Helper finds the URL of the Hill and Mountain Skills Handbook
on Mountain Training's website. This URL changes each time the manual is updated 
but the URL of the page containing the link to the manual is stable.

If for, whatever reason, the latest link is not found, then the class will
return the link to a locally-hosted copy of the PDF. This will probably be an
older version of the same handbook.

### Version ###
0.9.0

## Usage ##
There are two stages for using the MTHandbookUrl class: 

  1. Make sure configuration in src/Config.php is correct
  2. Create a Handbook object and call its only public method

### The Config Class ###
The Config Class contains four variables, only one of which should need updating
initially (LOCAL_PDF_URL), and one static method.

The variables are:

  * **MT_PAGE_URL**: This is the page that contains the live link. 
  * **LOCAL_PDF_URL**: This is the URL to the locally-hosted version of the PDF
    that will be returned if no URL is found on MT_PAGE_URL.
  * **FIND_LINE_REGEX**: This is the regular expression used to identify the
    line of HTML that contains the link to the latest URL. It shouldn't need 
    updating often, but if Mountain Training change their website it might.
  * **FIND_URL_REGEX**: This is the regular expression used to identify the URL
    from within the line of HTML identified by the previous regex. Again, it 
    shouldn't need updating, but if Mountain Training change their website to 
    include two PDF links on one line of HTML, it might.

The static method:

  * **reportUseOfLocalCopy()**: This method is called if the LOCAL_PDF_URL
    is used. This implies that Mountain Training website is offline or the page
    has been updated so the information in the Config File is out of date.
    This method is left blank so your own error tracking can be added. Being 
    aware the error is happening means Config Class can be updated if needed.

### Getting the latest handbook URL ###

At top of file calling the class add the following code:
```
use MTHandbookHelper\Handbook;
require_once 'Handbook.php';
```
The ```require_once``` line is not needed if you use an auto-loader

At some point in your code you need the following two lines:
```
$handbook = new Handbook();
$url = Handbook->getUrl();
```
```$url``` will contain the URL to the latest PDF version of the MT Handbook.

## Testing ##
The small script comes with a thorough test suit, partly because I'm currently
learning about Test Driven Development.

The test suit assumes you have *Composer* and *PHPUnit* installed.
First, set up the autoloader using Composer by running the command 
```
composer install
```
from the directory containing ```composer.json```. This will create the 
directory and file ```vendor/autoload.php```.

To run the test suite run the command:
```
phpunit --bootstrap vendor/autoload.php --testdox tests
```
The ```--testdox``` flag will let you see what tests are run. Their names are, 
hopefully, self explanatory.

### Fixing likely failure ###

If the test `'URL from config returns latest link'` fails the `MT_PAGE_URL` variable in `src/config.php` needs updating. Update it with a the URL from Mountain Training's website that contains the link to the lastest pdf of the MT Handbook.

