<?php
namespace MTHandbookHelper;

/**
 * Class to check a URL is valid and return the HTML from that URL
 */
class UrlHelper
{

    private $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function urlExists()
    {
        $curlSession = curl_init($this->url);
        curl_setopt($curlSession, CURLOPT_NOBODY, true); //only request HEAD

        curl_exec($curlSession);
        $returnCode = curl_getinfo($curlSession, CURLINFO_HTTP_CODE);
        curl_close($curlSession);

        $pageFoundCode = 200;
        return $returnCode == $pageFoundCode;
    }

    public function getHtml()
    {
        if ($this->urlExists()) {
            return $this->getHtmlBackend();
        } else {
            Throw new \Exception('Cannot find URL: ' . $this->url);
        }
    }

    private function getHtmlBackend()
    {
        $curlSession = curl_init($this->url);

        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true); //only return data, don't output html
        curl_setopt($curlSession, CURLOPT_FOLLOWLOCATION, true); //follow redirects

        $html = curl_exec($curlSession);
        curl_close($curlSession);

        return $html;
    }
}
