<?php
namespace MTHandbookHelper;

/**
 * Configure the behaviour of the MTHandbookHelper.
 * This is the only file that a third-party user needs to modify.
 * 
 * The comments should, hopefully, help with each line but there is
 * more information about this file in readme.md
 */
class Config
{

    public const MT_PAGE_URL = 'https://www.mountain-training.org/personal-skills/hill-and-mountain-skills'; //page containing link to latest URL for handbook PDF
    public const LOCAL_PDF_URL = 'http://assets.northernaspect.co.uk/pdfs/Hill_and_Mountain_Skill_Handbook.pdf'; //link locally-hosted copy; used if no link found
    public const FIND_LINE_REGEX = '/.*title="Hill and Mountain Skills Handbook"/'; //identifies the line of HTML containing the link
    public const FIND_URL_REGEX = '/http.*?pdf/'; //within the line of HTML already idenfied, identifies the latest URL for the PDF
    
    public static function reportUseOfLocalCopy()
    {
        // This method is called when the LOCAL_PDF_URL supplied above is used. 
        // 
        // The use of LOCAL_PDF_URL implies that either:
        //     * the Mountain Training website is offline, or
        //     * the page in question has been updated and the configuration
        //       in this file needs to be updated.
        // 
        // Write your own error reporting code in this method as appropriate 
        // for your system. Ideally, the webmaster wants to know that the 
        // the configuration needs updating.
    }
}
