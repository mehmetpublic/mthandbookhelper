<?php
namespace MTHandbookHelper;

require_once 'UrlHelper.php';
require_once 'Config.php';

/**
 * Class to find the URL of the Hill and Mountain Skills Handbook PDF on 
 * Mountain Training's website. The URL changes each time the manual is updated 
 * but the URL of the page containing the link to the manual is more stable. 
 * 
 * If a valid URL to the PDF cannot be found it will return a link to a locally-
 * hosted copy of the PDF. This is likely to be an out-of-date version.
 * 
 * The page containing the URL, on 2020-07-10, is:
 *  https://www.mountain-training.org/personal-skills/hill-and-mountain-skills
 * 
 *  $handbook = new Handbook();
 *  $url = $handbook->getUrl();
 * 
 * The regular expressions, websites etc are defined in Config.php
 */
class Handbook
{

    private $webpage;
    private $handbookUrl;

    public function __construct($url = null)
    {
        if (is_null($url)) {
            //Allows config file to be overridden
            //Also enables testing of wrong URLs etc.
            $url = Config::MT_PAGE_URL;
        }
        $this->webpage = new UrlHelper($url);
        $this->findHandbookHref();
    }

    public function getUrl()
    {
        return $this->handbookUrl;
    }

    private function findHandbookHref()
    {
        try {
            $pageHtml = $this->webpage->getHtml();
            $lineWithLink = $this->getMatch(Config::FIND_LINE_REGEX, $pageHtml);
            $this->handbookUrl = $this->getMatch(Config::FIND_URL_REGEX, $lineWithLink);
            $this->useLocalCopyIfNotPdf();
        } catch (\Exception $ex) {
            unset($ex);
            $this->useLocalCopy();
        }
    }

    private function getMatch($regex, $haystack)
    {
        $matches = array();
        preg_match($regex, $haystack, $matches);
        return $matches[0];
    }

    private function useLocalCopyIfNotPdf()
    {
        $needle = 'pdf';
        if (strpos($this->handbookUrl, $needle) === false) {
            $this->useLocalCopy();
        }
    }

    private function useLocalCopy()
    {
        $this->handbookUrl = Config::LOCAL_PDF_URL;
        Config::reportUseOfLocalCopy();
    }
}
