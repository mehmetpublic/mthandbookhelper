<?php
declare(strict_types=1);
namespace MTHandbookHelper;

use PHPUnit\Framework\TestCase;

final class ConfigTest extends TestCase
{

    public function testIsRegexDetectsBadRegEx()
    {
        $this->assertFalse($this->isRegEx('not valid regex'));
    }

    public function testFindLineIsRegex()
    {
        $this->assertTrue($this->isRegEx(Config::FIND_LINE_REGEX));
    }

    public function testFindUrlIsRegex()
    {
        $this->assertTrue($this->isRegEx(Config::FIND_URL_REGEX));
    }

    public function testLocalCopyUrlCouldBeValid()
    {
        $this->assertTrue($this->isValidUrl(Config::LOCAL_PDF_URL));
    }
    
    public function testPageUrlCouldBeValid()
    {
        $this->assertTrue($this->isValidUrl(Config::MT_PAGE_URL));
    }

    private function isRegEx($string)
    {
        //based on: https://stackoverflow.com/questions/8825025/test-if-a-regular-expression-is-a-valid-one-in-php
        return @preg_match($string, '') !== false;
    }
    
    private function isValidUrl($string)
    {
        return filter_var($string, FILTER_VALIDATE_URL) !== false;
    }
}
