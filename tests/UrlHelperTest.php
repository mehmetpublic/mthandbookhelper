<?php
declare(strict_types=1);
namespace MTHandbookHelper;

use PHPUnit\Framework\TestCase;

final class UrlHelperTest extends TestCase
{

    private $url;
    private $urlHelper;

    public function testUrlExistsFindsExistingWebpage()
    {
        $this->setUpGoodUrl();
        $this->assertTrue($this->urlHelper->urlExists());
    }

    public function testUrlExistsDoesNotFindBadUrl()
    {
        $this->setUpBadUrl();
        $this->assertFalse($this->urlHelper->urlExists());
    }

    public function testGetHtmlReturnsWebpage()
    {
        $this->setUpGoodUrl();
        $regexNeedle = '/<h1>Example Domain<\/h1>/';
        $haystack = $this->urlHelper->getHtml();
        $this->assertMatchesRegularExpression($regexNeedle, $haystack);
    }

    public function testGetHtmlThrowsExceptionForBadUrl()
    {
        $this->setUpBadUrl();
        $this->expectException('\\Exception');
        $this->urlHelper->getHtml();
        $this->fail('Cannot find url exception expected');
    }

    private function setUpGoodUrl()
    {
        $this->url = 'http://example.com/';
        $this->urlHelper = new UrlHelper($this->url);
    }

    private function setUpBadUrl()
    {
        $this->url = 'http://exampleFAIL.com/';
        $this->urlHelper = new UrlHelper($this->url);
    }
}
