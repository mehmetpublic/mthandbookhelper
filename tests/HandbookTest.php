<?php
declare(strict_types=1);
namespace MTHandbookHelper;

use PHPUnit\Framework\TestCase;

final class HandbookTest extends TestCase
{

    public function testUrlFromConfigReturnsLatestLink()
    {
        $handbook = new Handbook();

        //The $expected url may change if a new version is uploaded, so if this test fails check the pdf url manually
        $expected = 'http://www.mountain-training.org/Content/Uploaded/Downloads/MLT/1251671d-d025-4bea-894d-32ad7776808d.pdf';
        $actual = $handbook->getUrl();
        $this->assertEquals($expected, $actual);
    }

    public function testInvalidUrlReturnsBackup()
    {
        $badUrl = 'https://www.mountain-training.org/personal-skills/hill-and-mountain-sk';
        $handbook = new Handbook($badUrl);

        $expected = Config::LOCAL_PDF_URL;
        $actual = $handbook->getUrl();
        $this->assertEquals($expected, $actual);
    }

    public function testNoLinkOnPageReturnsBackup()
    {
        $url = 'http://example.com';
        $handbook = new Handbook($url);

        $expected = Config::LOCAL_PDF_URL;
        $actual = $handbook->getUrl();
        $this->assertEquals($expected, $actual);
    }
}
